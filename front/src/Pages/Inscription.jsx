import React, { useState } from 'react';
import { Form, Input, Button } from 'antd';
import useAuthContex from '../contex/AuthContex';

const Inscription = () => {
  const [email, setEmail] = useState('');
  const [mdp, setMdp] = useState('');
  const { dispatchAPI } = useAuthContex();
  const onSubmit = async (e) => {
    e.preventDefault();
    const data = {
      email,
      mdp
    };
    await dispatchAPI('inscription', data);
  };
  return (
    <div>
      <h1>Inscription </h1>
      <Form action="">
        <label>Pseudo: </label>
        <Input
          type="text"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <br />
        <br />
        <label>mot de passe : </label>
        <Input
          type="password"
          id="mdp"
          value={mdp}
          onChange={(e) => setMdp(e.target.value)}
        />
        <br />
        <br />
        <Button className="btn btn-success start" onClick={onSubmit}>
          {' '}
          Enregistrer
        </Button>
      </Form>
    </div>
  );
};
export default Inscription;
