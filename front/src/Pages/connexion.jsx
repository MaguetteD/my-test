import React, { useState } from 'react';
import { Form, Input, Button } from 'antd';
import useAuthContext from '../contex/AuthContex';

const Connexion = () => {
  const { dispatchAPI } = useAuthContext();
  const [email, setEmail] = useState('');
  const [mdp, setMdp] = useState('');
  const onSubmit = async (e) => {
    e.preventDefault();
    const data = {
      email,
      mdp
    };
    await dispatchAPI('login', data);
  };
  return (
    <div>
      <h1>connexion </h1>
      <Form action="">
        <label>Pseudo : </label>
        <Input
          type="text"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <br />
        <br />
        <label>mot de passe : </label>
        <Input
          type="password"
          id="mot de passe"
          value={mdp}
          onChange={(e) => setMdp(e.target.value)}
        />
        <br />
        <br />
        <Button className="btn btn-success start" onClick={onSubmit}>
          {' '}
          connexion
        </Button>
      </Form>
    </div>
  );
};
export default Connexion;
