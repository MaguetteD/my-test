import express, { json } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import db from './baseDeDonnee/data_config';
import Lister from './routes/users/Lister';
import login from './routes/users/login';
import inscription from './routes/users/inscription';

const app = express();
db();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(json());
app.use('/login', login);
app.use('/Lister', Lister);
app.use('/inscription', inscription);

app.get('/', (requete, resultat) => {
  resultat.send('ok');
});

app.get('/deconnexion', (req, response) => {
  response.json({ deconnexion: 'vous êtes déconnecté' });
});
app.listen(4000, () => {
  // console.log('connecté');
});
