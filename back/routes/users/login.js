import jwt from 'jsonwebtoken';
import validator from 'validator';
import bcrypt from 'bcrypt';
import modeleUser from '../../baseDeDonnee/dbschema';

const postLogin = async (requete, resp, next) => {
  const email = requete.body.email;
  const mdp = requete.body.mdp;
  try {
    if (!validator.isEmail(email) || validator.isEmpty(mdp) || mdp === ' ') {
      return resp.json('email ou mdp non valide');
    }
    const result = await modeleUser.findOne({ email });
    if (!result) {
      return resp.json('email existe pas');
    }

    if (!(await bcrypt.compare(mdp, result.mdp))) {
      return resp.json('Erreur problem de login');
    }
    jwt.sign(
      {
        data: result
      },
      'secret',
      { expiresIn: '1h' },
      (err, token) => {
        if (err) return resp.json('création non effectuée');

        return resp.json(token);
      }
    );
  } catch (e) {
    return next(e);
  }
};

export default postLogin;
