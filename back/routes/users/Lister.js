import jwt from 'jsonwebtoken';
import modeleUser from '../../baseDeDonnee/dbschema';

const getListe = (requete, resp, next) => {
  const token = requete.headers['x-access-token'];
  try {
    if (!token) return resp.json('pas de token trouvé');
    jwt.verify(token, 'secret', async (err) => {
      if (!err) {
        const result = await modeleUser.find();
        if (result) {
          return resp.json(result);
        }
        return resp.json('pas dutilisateur');
      }
      return resp.json('connectez-vous pour lister les utilisateurs');
    });
  } catch (e) {
    return next(e);
  }
};

export default getListe;
