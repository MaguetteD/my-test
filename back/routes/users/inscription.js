import validator from 'validator';
import bcrypt from 'bcrypt';
import ModeleUser from '../../baseDeDonnee/dbschema';

const saltRounds = 10;
const postInscription = async (requete, res, next) => {
  try {
    const email = requete.body.email;
    const mdp = requete.body.mdp;
    if (!validator.isEmail(email) || validator.isEmpty(mdp) || mdp === ' ') {
      return res.json('email ou mdp non valide');
    }
    const result = await ModeleUser.findOne({ email });
    if (result) {
      return res.json('user existe deja');
    }
    const data = await new ModeleUser({
      email,
      mdp
    });
    bcrypt.hash(mdp, saltRounds, (erre, hash) => {
      data.mdp = hash;
      data.save((err) => {
        if (err) {
          return res.json('non ajouté');
        }
        return res.json('sucess');
      });
    });
  } catch (e) {
    return next(e);
  }
  // return false;
};

export default postInscription;
